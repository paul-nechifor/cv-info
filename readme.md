# CV Info

Generate CV data from a YAML file.

![package cover](screenshot.png)

## License

MIT
